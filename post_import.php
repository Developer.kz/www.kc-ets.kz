<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Отправка данных..</title>
</head>
<body>
<p>Импорт данных..</p>
<?php
//echo  sys_get_temp_dir();
require_once 'lib/func.inc';
if ($_FILES["edtFile"]["error"] > 0) die ('Ошибка файла: '.$_FILES["edtFile"]["error"].'</br>');
$xml = simplexml_load_file($_FILES["edtFile"]["tmp_name"]) or die("XML Error: Cannot create object");
if ($xml === false) {
	foreach(libxml_get_errors() as $error) {
		echo "<br>", $error->message;
	}
	die ("Failed loading XML: ");
}
else
	{
	echo $xml->getName().'</br>';
	$dt_string = $xml->attributes()->ActualData;
	$datetime = new DateTime($dt_string, new DateTimeZone('Asia/Almaty'));
	echo $datetime->format('Y-m-d H:i:s').'</br>';
	echo 'Очистка таблиц..</br>';
	xml_copy_all();
	xml_truncate_all();
	echo 'Добавление import..</br>';
	echo '</br>';
	xml_add_root($datetime->format('Y-m-d H:i:s'), $_FILES['edtFile']['tmp_name'], $_FILES['edtFile']['name']);
	
	echo 'Цикл..</br>';
	foreach($xml->children() as $brokers) {
		echo '-------------------------Брокер--------------------------------</br>';
		echo 'Брокер: '.$brokers->FullName.'</br>';
		echo 'БИН: '.$brokers->BIN.'</br>';
		echo 'ВСЕГО: '.$brokers->Total_amount.'</br>';
		echo '</br>';

		xml_add_broker($brokers->FullName, $brokers->BIN, $brokers->Total_amount);

		foreach ($brokers->children() as $cust)	{
			echo '-------------------------клиент--------------</br>';
			echo 'Наименование клиента: '.$cust->FullName.'</br>';
			echo 'БИН: '.$cust->BIN.'</br>';
			$cust_part_code = '';
			$cust_legal_code = '';
			$cust_acc_code = '';
			$cust_total = 0;
			foreach ($cust->children() as $account)	{
				if (!empty($account->Part_code) && !empty($account->Legal_code) && !empty($account->Acc_code) && !empty($account->Total_amount))
				{
					echo 'Код брокера: '.$account->Part_code.'</br>';
					$cust_part_code = $account->Part_code;
					echo 'Код торг. счёта: '.$account->Legal_code.'</br>';
					$cust_legal_code = $account->Legal_code;
					echo 'Код раздела регистра: '.$account->Acc_code.'</br>';
					$cust_acc_code = $account->Acc_code;
					echo 'Средства: '.$account->Total_amount.'</br>';
					$cust_total = (float)$account->Total_amount;
					xml_add_customer($cust->FullName, $cust->BIN, $cust_total, $cust_part_code, $cust_acc_code, $cust_legal_code);
				}
			}
		}
	}
	//$sql = 'INSERT INTO tbl_uploads(file,type,size) VALUES('.$file.','.$file_type.','.$file_size.')';
	$conn = iconnect();
	$res = $conn->query("CALL proc_xml_add_customer('$full_name','$BIN',$total,'$part_code','$acc_code','$legal_code')");
	mysqli_free_result($res);
	mysqli_close($conn);
	unlink($_FILES["edtFile"]["tmp_name"]);
	goto_page('cabinet.php?p=import_done');
	}
?>
</body>
</html>