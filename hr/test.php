<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title></title>

    <!-- Bootstrap -->
    <link href="bootstrap337/css/bootstrap.min.css" rel="stylesheet">

  </head>
  <body>

    <div class="container-fluid">
    
    <?php
    if (isset($_POST['edtAmount']))
    	foreach ($_POST['edtAmount'] as $key => $value) {
    		echo $value . "<br />";
	    }
    ?>
    
   <form id="frm" name="frm" method="post" action="test.php" enctype="multipart/form-data">
  	<!--  Тело страницы -->
  	<div class="row">
	  	<div class="col-md-8">
	  	
  	Прошу возвратить денежные средства, обязательства по перечислению которых учитываются на разделах клирингового регистра:
	<div class="table-responsive">
		<table class="table table-bordered" id="myTable" name="myTable">
    		<thead>
		      <tr class="success">
				<th>Код раздела</th>
				<th>Код лота</th>
				<th>Сумма, тенге</th>
				</tr>
		    </thead>
		    <tbody>
	  	<tr>
			    <td>
				</td>
		    	<td class="text-right">
					 <button type="button" onclick="funcSum()">Суммировать</button>
					 <b>Итого:</b>
					 <script>
						function funcSum() {
    					    var tot = 0;
    					    var val = 0;
    					    arr = document.getElementsByName('edtAmount[]');
    					    for	(var i = 0; i < arr.length; i++)	{
    					    	val = parseFloat(arr[i].value);
  					            tot += val; 
    					    }
    					    document.getElementById('edtSum').value = tot.toFixed(2);
						} 
					</script>
				</td>
				<td>
					<input type="text" class="form-control" id="edtSum" name="edtSum">
				</td>
		    </tr>
			</tbody>
  			</table>
		</div>
	  </div>
	  </div>
	<!--  Форма -->

<div class="row">
	<button type="button" id="myButton" class="btn btn-primary" autocomplete="off" onclick="AddRowClick()">
 		Добавить строку
	</button>

	<script>
	  function AddRowClick() {
		var table = document.getElementById("myTable");
		var tbody = document.getElementById("myTable").tBodies[0];
		var i = 0;
		var rows = 0;
		rows = document.getElementById("myTable").rows.length;
		var newrow = rows - 1;
		//$(table).find(tbody).append( "<tr><td>aaaa</td></tr>" );
		var row = table.insertRow(newrow);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		var cell3 = row.insertCell(2);
		document.getElementById("myTable").rows[newrow].cells[0].innerHTML = '<input type="text" class="typeahead" placeholder="Набирайте текст..." name="edtAccCode[]">';
		document.getElementById("myTable").rows[newrow].cells[1].innerHTML = '<input type="text" class="form-control" name="edtLotCode[]">';
		document.getElementById("myTable").rows[newrow].cells[2].innerHTML = '<input type="text" class="form-control" name="edtAmount[]">';
	  }
	</script>

</div>

<button type="submit" class="btn btn-default">Отправить</button>

</form>

</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="bootstrap337/js/bootstrap.min.js"></script>
  </body>
</html>