<?php
require_once 'func.inc';
//проверка переменной сессии logged, если её нет отправить пользователя на страницу входа
if (!isset($_SESSION['logged']))
	goto_page('login.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title></title>

    <!-- Bootstrap -->
    <link href="bootstrap337/css/bootstrap.min.css" rel="stylesheet">

  </head>
  <body>

    <div class="container-fluid">
    
    <!--  Меню -->
  	<div class="row">
    
	     <nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="/">БД Отдел кадров</a>
		      
		    </div>
		
		<!-- пункты меню -->
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		      
		       
		        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>  -->
		        <li><a href="?p=tree">Структура</a></li>
		      
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Отделы<span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="?p=create_dep">Создать</a></li>
		            <li><a href="?p=change_dep">Изменить</a></li>
		            <li><a href="?p=del_dep">Удалить</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="?p=view_all_dep">Просмотреть все</a></li>
		          </ul>
		        </li>
		        
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Должности<span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="?p=create_tit">Создать</a></li>
		            <li><a href="?p=change_tit">Изменить</a></li>
		            <li><a href="?p=del_tit">Удалить</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="?p=view_all_tit">Просмотреть все</a></li>
		          </ul>
		        </li>
		        
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Сотрудники<span class="caret"></span></a>
		          <ul class="dropdown-menu">
		             <li><a href="?p=create_emp">Создать</a></li>
		             <li><a href="?p=on_position">Назначить на должность</a></li>
		            <li><a href="?p=change_emp">Изменить</a></li>
		            <li><a href="?p=del_emp">Уволить</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="?p=view_all_emp">Просмотреть все</a></li>
		            <li><a href="?p=view_all_emp_by_tit">По должностям</a></li>
		          </ul>
		        </li>
		        
		      </ul>
		      <form class="navbar-form navbar-left" method="post" action="index.php" enctype="multipart/form-data">
		        <div class="form-group">
		          <input type="text" class="form-control" placeholder="Поиск" id="search_param" name="search_param">
		        </div>
		        <button type="submit" class="btn btn-default" id="search" name="search">Найти</button>
		      </form>
		      
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		    
  </div>
  <!--  Меню -->
  
  	<!--  Тело страницы -->
  	<div class="row">
	  	
	  	<?php
	  	//если параметр адресной строки p не существует, не было выбрано пункта меню
	  	if (!isset($_GET['p']))
	  		{
  			//если параметр для поиска существует, то произвести поиск и вывести результаты
	  		if (isset($_POST['search']))
	  		{
	  			echo '<div class="col-xs-12">';
	  			search_dep($_POST['search_param']);
	  			search_tit($_POST['search_param']);
	  			search_emp($_POST['search_param']);
	  		}
	  		else
	  		//в противном случае отобразить приветственный текст
		  		{
		  		echo '<div class="col-xs-6">';
				alert_success("Добро пожаловать в систему учёта персонала");
	  			}
	  		}
	  	else
	  		//если параметр адресной строки существует, был выбран пункт меню
	  		{
	  			//определяем что выбрал пользователь
	  		switch ($_GET['p'])
		  		{
		  		//создать подразделение, отобразить совмещенно страницу форму
	  			case 'create_dep':
	  				echo '<div class="col-xs-6">';
	  				require_once 'create_dep.php';
	  				break;
	  			//создать сотрудника, отобразить совмещенно страницу форму
	  			case 'create_emp':
	  				echo '<div class="col-xs-6">';
	  				require_once 'create_emp.php';
	  				break;
  				//создать должность, отобразить совмещенно страницу форму
	  			case 'create_tit':
	  				echo '<div class="col-xs-6">';
	  				require_once 'create_title.php';
	  				break;
  				//просмотр всех сотрудников
	  			case 'view_all_emp':
	  				echo '<div class="col-xs-12">';
	  				require_once 'view_all_emp.php';
	  				break;
  				//просмотр всех подразделений
	  			case 'view_all_dep':
	  				echo '<div class="col-xs-6">';
	  				require_once 'view_all_dep.php';
	  				break;
  				//просмотр всех должностей
	  			case 'view_all_tit':
	  				echo '<div class="col-xs-6">';
	  				require_once 'view_all_tit.php';
	  				break;
  				//назначить сотрудника на должность, отобразить совмещенно страницу форму
	  			case 'on_position':
	  				echo '<div class="col-xs-6">';
	  				require_once 'on_position.php';
	  				break;
  				//просмотр всех сотрудников с должностями
	  			case 'view_all_emp_by_tit':
	  				echo '<div class="col-xs-6">';
	  				require_once 'view_all_emp_by_tit.php';
	  				break;
  				//просмотр всей штатной структуры
	  			case 'tree':
	  				echo '<div class="col-xs-12">';
	  				require_once 'view_tree.php';
	  				break;
  				//изменить подразделение, отобразить совмещенно страницу форму
	  			case 'change_dep':
	  				echo '<div class="col-xs-6">';
	  				require_once 'change_dep.php';
	  				break;
  				//удалить подразделение, отобразить совмещенно страницу форму
	  			case 'del_dep':
	  				echo '<div class="col-xs-6">';
	  				require_once 'del_dep.php';
	  				break;
  				//изменить должность, отобразить совмещенно страницу форму
	  			case 'change_tit':
	  				echo '<div class="col-xs-6">';
	  				require_once 'change_tit.php';
	  				break;
  				//удалить должность, отобразить совмещенно страницу форму
	  			case 'del_tit':
	  				echo '<div class="col-xs-6">';
	  				require_once 'del_tit.php';
	  				break;
  				//изменить реквизиты сотрудника, отобразить совмещенно страницу форму
	  			case 'change_emp':
	  				echo '<div class="col-xs-6">';
	  				require_once 'change_emp.php';
	  				break;
  				//удалить сотрудника, отобразить совмещенно страницу форму
	  			case 'del_emp':
	  				echo '<div class="col-xs-6">';
	  				require_once 'del_emp.php';
	  				break;
	  			}
		  	}
	  	?>	  	
	  	
	  </div>
	</div>
	<!--  Форма -->

</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="bootstrap337/js/bootstrap.min.js"></script>
  </body>
</html>