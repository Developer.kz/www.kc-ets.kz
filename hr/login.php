<?php 
//подключение собственной библиотеки функций
require_once 'func.inc';
?>
<!-- HTML разметки -->
<!DOCTYPE html> 
<html lang="en">
   <!--  Заголовок страницы -->
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title></title>

	<!-- подключение стиля библиотеки Bootstrap (Twitter), используется для отображения элементов формы, разметок формы и таблиц -->
    <!-- Bootstrap -->
    <link href="bootstrap337/css/bootstrap.min.css" rel="stylesheet">

  </head>
  
  <!--  Тело страницы -->
  <body>
	
	<!--  Контейнер разметки страницы -->
    <div class="container-fluid">
    
  	<div class="row"> <!--  Разметка -->
  	<!--  Форма входа, значения для обаботки отправляются в страницу check_login.php -->
<form method="post" action="check_login.php" id="frm" name="frm">
	<div class="row"> <!--  Разметка -->
	<div class="col-md-1"> <!--  Разметка -->
	</div>
	  <div class="col-md-3"> <!--  Разметка -->
		<h3>Вход в БД Отдел кадров</h3>  <!--  Заголовок формы -->
			<div class="form-group"> <!--  Разметка -->
				<label for="frm_login">Логин</label> <!--  Метка поля ввода логина --> 
				<input type="text" class="form-control" name="login" id="login" placeholder="..."> <!--  Поле ввода логина -->
			</div>
			<div class="form-group"> <!--  Разметка -->
				<label for="exampleInputPassword1">Пароль</label> <!--  Метка поля ввода пароль --> 
				<input type="password" class="form-control" name="pwd" id="pwd" placeholder="..."> <!--  Поле ввода пароль -->
			</div>
			<button type="submit" class="btn btn-default">Войти</button> <!--  Кнопка отправки данных на сервер --> 
	  	</div>
	</div>
</form>  
</div>

    <!-- подключение библиотеки jQuery от Google, необходим для исполнения Bootstrap скриптов -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<!-- подключение библиотеки JavaScript от Bootstrap (Twitter), используется для отображения элементов формы, разметок формы и таблиц -->
    <script src="bootstrap337/js/bootstrap.min.js"></script>
  </body>
</html>