<?php
require_once 'func.inc';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title></title>

    <!-- Bootstrap -->
    <link href="bootstrap337/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <div class="container-fluid">
    
    
  	<!--  Форма -->
  	<div class="row">

	 <div class="col-md-4">
	 	<img src="banner.jpg" alt="" class="img-rounded">
	 </div>
	 <div class="col-md-4">
	 	<p>
	 	Здравствуйте! Наша система управления персоналом "БД Отдел кадров" позволяет организации любого уровня
	 	вести учёт своего персонала эффективно и просто. Для получения доступа в систему Вам необходимо произвести
	 	оплату и получить логин и пароль. 
	 	</p>
		<form action="login.php">
		  <div class="form-group">
		    <label for="exampleInputEmail1">Сумма оплаты, в тг.</label>
		    <input type="text" class="form-control" id="DepName" name="DepName" placeholder="Сумма">
		  </div>
		  <div class="checkbox">
		    <label>
		      <input type="checkbox">Согласен с условиями использования ПО
		    </label>
		  </div>
			<row>
			  <div class="col-md-10"></div>
			  <div class="col-md-2">
			    <div class="form-group">
				  	<button type="submit" class="btn btn-success">Оплатить</button>
				  </div>
			  </div>
		  </row>
		
		</form>
	 </div>
	</div>
	<!--  Форма -->

</div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap337/js/bootstrap.min.js"></script>
  </body>
</html>
