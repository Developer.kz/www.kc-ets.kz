<h4>Назначение на должность</h4>
<hr>

<form id="frm" name="frm" method="post" action="post_position.php" enctype="multipart/form-data">

  <div class="form-group">
    <label for="exampleInputPassword1">Сотрудник</label>
    <select class="form-control" name="Person" id="Person">
	  <?php
	  AllEmpListByFIO();
	  ?>
	</select>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Подразделение назначения</label>
    <select class="form-control" name="Dep" id="Dep">
      <?php
      AllDepListByNam();
      ?>
	</select>
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">Должность</label>
     <select class="form-control" name="Title" id="Title">
      <?php
      AllTitList();
      ?>
	</select>
  </div>
   
	<row>
	  <div class="col-md-10"></div>
	  <div class="col-md-2">
	    <div class="form-group">
		  	<button type="submit" class="btn btn-success">Назначить</button>
		  </div>
	  </div>
  </row>

</form>