<?php
require_once 'func.inc';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body>
<?php
if (isset($_POST['LoadEmp']))
{
	goto_page('/?p=change_emp&empid='.$_POST['EmpId']);
}
if (isset($_POST['Update']))
{
	db_UpdateEmp(
			$_POST['EmpId'],
			$_POST['LastName'],
			$_POST['FirstName'],
			$_POST['MiddleName'],
			$_POST['Sex'],
			$_POST['Day'],
			$_POST['Month'],
			$_POST['Year'],
			$_POST['IIN'],
			$_POST['address'],
			$_POST['Phone'],
			$_POST['Education']
			);
	goto_page('/');
}


?>
</body>
</html>