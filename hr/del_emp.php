<h4>Уволить сотрудника</h4>
<hr>

<form id="frm" name="frm" method="post" action="post_del_emp.php" enctype="multipart/form-data">

	<div class="form-group">
	    <label for="exampleInputFile">Выберите сотрудника</label>
			<select class="form-control" name="EmpId" id="EmpId">
		      <?php
		      AllEmpListByFIOForChange();
		      ?>
			</select>
		</div>
  
  <div class="form-group">
  <div class="checkbox">
    <label>
      <input type="checkbox" id="Agree" name="Agree">Подтверждаю увольнение
    </label>
  </div>
  </div>
  
	<row>
	  <div class="col-md-10"></div>
	  <div class="col-md-2">
	    <div class="form-group">
		  	<button type="submit" class="btn btn-danger">Удалить</button>
		  </div>
	  </div>
  </row>

  <div class="form-group">
  		<span class="label label-info">Внимание! Все данные сотрудника будут удалены.</span>
  </div>

</form>