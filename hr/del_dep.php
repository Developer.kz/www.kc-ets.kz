<h4>Удалить подразделение</h4>
<hr>

<form id="frm" name="frm" method="post" action="post_del_dep.php" enctype="multipart/form-data">

	<div class="form-group">
    <label for="exampleInputFile">Подразделение для удаления</label>
     <select class="form-control" name="DepId" id="DepId">
      <?php
      AllDepListByNam();
      ?>
	</select>
  </div>
  <div class="form-group">
  <div class="checkbox">
    <label>
      <input type="checkbox" id="Agree" name="Agree">Подтверждаю удаление
    </label>
  </div>
  </div>
  
	<row>
	  <div class="col-md-10"></div>
	  <div class="col-md-2">
	    <div class="form-group">
		  	<button type="submit" class="btn btn-danger">Удалить</button>
		  </div>
	  </div>
  </row>

  <div class="form-group">
  		<span class="label label-info">Внимание! Назначения сотрудников данного подразделения будут удалены.</span>
  </div>

</form>