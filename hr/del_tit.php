<h4>Удалить должность</h4>
<hr>

<form id="frm" name="frm" method="post" action="post_del_tit.php" enctype="multipart/form-data">

	<div class="form-group">
    <label for="exampleInputFile">Должность для удаления</label>
     <select class="form-control" name="TitleId" id="TitleId">
      <?php
      AllTitlesList();
      ?>
	</select>
  </div>
  <div class="form-group">
  <div class="checkbox">
    <label>
      <input type="checkbox" id="Agree" name="Agree">Подтверждаю удаление
    </label>
  </div>
  </div>
  
	<row>
	  <div class="col-md-10"></div>
	  <div class="col-md-2">
	    <div class="form-group">
		  	<button type="submit" class="btn btn-danger">Удалить</button>
		  </div>
	  </div>
  </row>

  <div class="form-group">
  		<span class="label label-info">Внимание! Назначения сотрудников на данной должности будут удалены.</span>
  </div>

</form>