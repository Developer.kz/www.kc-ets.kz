<h4>Изменить реквизиты сотрудника</h4>
<hr>
<form id="frm" name="frm" method="post" action="post_change_emp.php" enctype="multipart/form-data" class="form-horizontal">

	<div class="col-sm-12">
		<div class="form-group">
	    <label for="exampleInputFile">Выберите сотрудника</label>
			<select class="form-control" name="EmpId" id="EmpId">
		      <?php
		      AllEmpListByFIOForChange();
		      ?>
			</select>
		</div>
	</div>
	<row>
	  <div class="col-md-9"></div>
	  <div class="col-md-1">
	    <div class="form-group">
		  	<button type="submit" class="btn btn-primary" id="LoadEmp" name="LoadEmp">Загрузить данные</button>
		  </div>
	  </div>
  </row>
  
  <div class="form-group">
    <label for="exampleInputEmail1" class="col-sm-3 control-label">Фамилия</label>
     <div class="col-sm-7">
    	<input type="text" class="form-control" id="LastName" name="LastName" placeholder="Фамилия"
    	value="<?php echo GetEmpDetail($_GET['empid'], 'lastname'); ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">Имя</label>
    <div class="col-sm-7">
    	<input type="text" class="form-control" id="FirstName" name="FirstName" placeholder="Имя"
    	value="<?php echo GetEmpDetail($_GET['empid'], 'firstname'); ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">Отчество</label>
    <div class="col-sm-7">
    	<input type="text" class="form-control" id="MiddleName" name="MiddleName" placeholder="Отчество"
    	value="<?php echo GetEmpDetail($_GET['empid'], 'middlename'); ?>">
     </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">Пол</label>
    <div class="col-sm-7">
    	<select class="form-control" id="Sex" name="Sex">
    		<?php
    		if (GetEmpDetail($_GET['empid'], 'sex') == 'Мужской')
    			echo '<option selected>Мужской</option><option>Женский</option>';
    		else
    			echo '<option>Мужской</option><option selected>Женский</option>';
    		?>
		</select>
     </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">Дата рождения</label>
    <div class="col-sm-7">
    	
    	<div class="col-md-4">
    	<label for="exampleInputPassword1" class="col-sm-3 control-label">День</label>
	    	<select class="form-control" id="Day" name="Day">
	    	<?php
	    	$dt = strtotime(GetEmpDetail($_GET['empid'], 'birthdate'));
	    	$day = date( "d", $dt);
	    	for ($i = 1; $i <= 31; $i++)
		    	{
		    	if ($day == $i)
	    			echo '<option selected>'.$i.'</option>';
		    	else
		    		echo '<option>'.$i.'</option>';
		    	}
    		?>
			</select>
    	</div>
    	
    	<div class="col-md-4">
    	<label for="exampleInputPassword1" class="col-sm-3 control-label" >Месяц</label>
	    	<select class="form-control" id="Month" name="Month">
		    	<?php
		    	$dt = strtotime(GetEmpDetail($_GET['empid'], 'birthdate'));
		    	$mon = date( "m", $dt);
				SelectedBirthMonth($mon);
   				?>
			</select>
    	</div>
    	
    	<div class="col-md-4">
    	<label for="exampleInputPassword1" class="col-sm-3 control-label" >Год</label>
	    	<select class="form-control" id="Year" name="Year">
				<?php
		    	$dt = strtotime(GetEmpDetail($_GET['empid'], 'birthdate'));
		    	$year = date( "Y", $dt);
				YearsSelected($year);
	    		?>
			</select>
    	</div>
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">ИИН</label>
    <div class="col-sm-7">
    	<input type="text" class="form-control" id="IIN" name="IIN" placeholder="ИИН"
    	value="<?php echo GetEmpDetail($_GET['empid'], 'iin'); ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">Адрес проживания</label>
    <div class="col-sm-7">
    	<input type="text" class="form-control" id="address" name="address" placeholder="Адрес проживания"
    	value="<?php echo GetEmpDetail($_GET['empid'], 'address'); ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">Телефон</label>
    <div class="col-sm-7">
    	<input type="text" class="form-control" id="Phone" name="Phone" placeholder="Телефон"
    	value="<?php echo GetEmpDetail($_GET['empid'], 'phone'); ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">Образование</label>
    <div class="col-sm-7">
    	<input type="text" class="form-control" id="Education" name="Education"  placeholder="Образование"
    	value="<?php echo GetEmpDetail($_GET['empid'], 'education'); ?>">
    </div>
  </div>
  
  <row>
	  <div class="col-md-10"></div>
	  <div class="col-md-2">
	    <div class="form-group">
		  	<button type="submit" class="btn btn-success" id="Update" name="Update">Обновить</button>
		  </div>
	  </div>
  </row>
  
</form>