<h4>Создать сотрудника</h4>
<hr>
<form id="frm" name="frm" method="post" action="post_emp.php" enctype="multipart/form-data" class="form-horizontal">

  <div class="form-group">
    <label for="exampleInputEmail1" class="col-sm-3 control-label">Фамилия</label>
     <div class="col-sm-7">
    	<input type="text" class="form-control" id="LastName" name="LastName" placeholder="Фамилия">
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">Имя</label>
    <div class="col-sm-7">
    	<input type="text" class="form-control" id="FirstName" name="FirstName" placeholder="Имя">
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">Отчество</label>
    <div class="col-sm-7">
    	<input type="text" class="form-control" id="MiddleName" name="MiddleName" placeholder="Отчество">
     </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">Пол</label>
    <div class="col-sm-7">
    	<select class="form-control" id="Sex" name="Sex">
		  	<option>Мужской</option>
			<option>Женский</option>
		</select>
     </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">Дата рождения</label>
    <div class="col-sm-7">
    	
    	<div class="col-md-4">
    	<label for="exampleInputPassword1" class="col-sm-3 control-label">День</label>
	    	<select class="form-control" id="Day" name="Day">
			  	<?php
			  	Days();
			  	?>
			</select>
    	</div>
    	
    	<div class="col-md-4">
    	<label for="exampleInputPassword1" class="col-sm-3 control-label" >Месяц</label>
	    	<select class="form-control" id="Month" name="Month">
			  	<option value="1">Январь</option>
			  	<option value="2">Февраль</option>
			  	<option value="3">Март</option>
			  	<option value="4">Апрель</option>
			  	<option value="5">Май</option>
			  	<option value="6">Июнь</option>
			  	<option value="7">Июль</option>
			  	<option value="8">Август</option>
			  	<option value="9">Сентябрь</option>
			  	<option value="10">Октябрь</option>
			  	<option value="11">Ноябрь</option>
			  	<option value="12">Декабрь</option>
			</select>
    	</div>
    	
    	<div class="col-md-4">
    	<label for="exampleInputPassword1" class="col-sm-3 control-label" >Год</label>
	    	<select class="form-control" id="Year" name="Year">
	    	<?php
	    	Years();
	    	?>
			</select>
    	</div>
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">ИИН</label>
    <div class="col-sm-7">
    	<input type="text" class="form-control" id="IIN" name="IIN" placeholder="ИИН">
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">Адрес проживания</label>
    <div class="col-sm-7">
    	<input type="text" class="form-control" id="address" name="address" placeholder="Адрес проживания">
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">Телефон</label>
    <div class="col-sm-7">
    	<input type="text" class="form-control" id="Phone" name="Phone" placeholder="Телефон">
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="col-sm-3 control-label">Образование</label>
    <div class="col-sm-7">
    	<input type="text" class="form-control" id="Education" name="Education"  placeholder="Образование">
    </div>
  </div>
  
  <row>
	  <div class="col-md-10"></div>
	  <div class="col-md-2">
	    <div class="form-group">
		  	<button type="submit" class="btn btn-success">Создать</button>
		  </div>
	  </div>
  </row>
  
</form>