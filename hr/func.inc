<?php
//библиотека функций пользователя

//установка временной зоны Казахстан, Астана
date_default_timezone_set("Asia/Almaty");
//старт сессии
session_start();
//отображения ошибок
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);

//подключение страницы параметров для подключения к базе данных
require_once 'config.php';

//функция подключения к БД, возвращает переменную подключения, через которую уже можно делать запрос в таблицы
function iconnect()  {
	//глобальная переменная из config.php
	global $db;
	//создание подключения с параметрами из config.php, сервер, пользователь, пароль и база данных
	$mysqli = new mysqli($db['server'], $db['user'], $db['pwd'], $db['db']);
	//проверка подключения
	if (mysqli_connect_errno())
		die('Connect failed: '.mysqli_connect_error());
	return $mysqli;
}

//функция перехода пользователя на определённую страницу, аргумент название файла страницы
function goto_page($page_name)	{
	//используется JavaScript внутри PHP
	echo '<script type="text/javascript">window.location = "'.$page_name.'"</script>';
}

//функция добавления подразделения в базу данных, аргументы название и ID управляющего подразделения
function db_AddDep($DepName, $HeadDepId)	{
	//подключение к бд
	$conn = iconnect();
	//отправка запроса на вставку записи в таблицу
	$query = 'insert into department (DepName, HeadDepID) values (\''.$DepName.'\', '.$HeadDepId.');';
	//выполнение запроса на сервере баз данных
	$res = $conn->query($query);
	//проверка результата
	if(!$res)	{
		alert_danger('Ошибка записи в БД запрос: </br>'.$query);
		die();
		}
	//закрытие содинения
	mysqli_close($conn);
}

//функция Добавления сотрудника
function db_AddEmp(
		$LastName,
		$FirstName,
		$MiddleName,
		$Sex,
		$Day,
		$Month,
		$Year,
		$IIN,
		$address,
		$Phone,
		$Education
		)	{
	$conn = iconnect();
	$date = $Day.'.'.$Month.'.'.$Year; 
	//$date = date('d/m/Y', strtotime($date));
	$query = 'insert into employees (
		LastName,	
		FirstName,
		MiddleName,
		Sex,
		birthdate,
		IIN,
		address,
		Phone,
		Education
		) values (
		\''.$LastName.'\',
		\''.$FirstName.'\',
		\''.$MiddleName.'\',
		\''.$Sex.'\',
		\''.$date.'\',
		\''.$IIN.'\',
		\''.$address.'\',
		\''.$Phone.'\',
		\''.$Education.'\');';
	$res = $conn->query($query);
	if(!$res)	{
		alert_danger('Ошибка записи в БД запрос: </br>'.$query);
		//остановить загрузку страницы
		die();
	}
	mysqli_close($conn);
}

//функция Добавления Должности
function db_AddTit($TitName)	{
	$conn = iconnect();
	$query = 'insert into titles (name) values (\''.$TitName.'\');';
	$res = $conn->query($query);
	if(!$res)	{
		alert_danger('Ошибка записи в БД запрос: </br>'.$query);
		die();
	}
	mysqli_close($conn);
}

//функция вывода таблицы всех сотрудников
function view_all_emp() {
	$conn = iconnect();
	$k = 1;
	if ($res = $conn->query('SELECT * from employees order by created;'))	{
		echo '
	
	<div class="table-responsive">
  		<table class="table table-bordered">
    		<thead>
		      <tr class="success">
				<th>#</th>
		        <th><small>Фамилия</small></th>
		        <th><small>Имя</small></th>
				<th><small>Отчество</small></th>
				<th><small>Пол</small></th>
				<th><small>Дата рождения</small></th>
				<th><small>ИИН</small></th>
				<th><small>Адрес проживания</small></th>
				<th><small>Телефон</small></th>
				<th><small>Образование</small></th>
				<th><small>Дата учёта</small></th>
		      </tr>
		    </thead>
		    <tbody>';
		while ($row = $res->fetch_row())
			{
			$date = new DateTime($row[10]);
			$date1 = new DateTime($row[9]);
			echo
			'
				<tr>
					<td>'.$k++.'</td>
					<td>'.$row[1].'</td>
					<td>'.$row[2].'</td>
					<td>'.$row[3].'</td>
					<td>'.$row[5].'</td>
					<td>'.$date1->format('d/m/Y').'</td>
					<td>'.$row[4].'</td>
					<td>'.$row[6].'</td>
					<td>'.$row[7].'</td>
					<td>'.$row[8].'</td>
					<td>'.$date->format('d/m/Y').'</td>
				</tr>
				';
			}
		echo '
		    </tbody>
  		</table>
	</div>';
	}
	mysqli_close($conn);
}

//функция вывод сообщения удачной операции
function alert_success($msg)	{
	echo '<div class="alert alert-success" role="alert">'.$msg.'</div>';
}

//функция вывод сообщения неудачной операции
function alert_danger($msg)	{
	echo '<div class="alert alert-danger" role="alert">'.$msg.'</div>';
}

//функция вывода списка всех сотрудников по ФИО
function AllEmpListByFIO() {
	$conn = iconnect();
	if ($res = $conn->query('SELECT id, trim(concat(lastname, " ", firstname, " ", middlename)) fio from employees ORDER by fio;'))	
		{
		while ($row = $res->fetch_row())
			{
			echo
			'<option value="'.$row[0].'">'.$row[1].'</option>'.PHP_EOL;
			}
		}
	mysqli_close($conn);
}

//функция вывода списка всех сотрудников по ФИО для изменения
function AllEmpListByFIOForChange() {
	$conn = iconnect();
	if ($res = $conn->query('SELECT id, trim(concat(lastname, " ", firstname, " ", middlename)) fio from employees ORDER by fio;'))
		while ($row = $res->fetch_row())
			if (isset($_GET['empid']))
				{
				if ($row[0] == $_GET['empid'])
					{
					echo '<option value="'.$row[0].'" selected>'.$row[1].'</option>'.PHP_EOL;
					}
				else
					echo '<option value="'.$row[0].'">'.$row[1].'</option>'.PHP_EOL;
				}
			else 	
				echo '<option value="'.$row[0].'">'.$row[1].'</option>'.PHP_EOL;
	mysqli_close($conn);
}

//функция вывода списка подразделений по имени
function AllDepListByNam() {
	$conn = iconnect();
	if ($res = $conn->query('SELECT id, DepName from department order by DepName;'))
	{
		while ($row = $res->fetch_row())
		{
			echo
			'<option value="'.$row[0].'">'.$row[1].'</option>'.PHP_EOL;
		}
	}
	mysqli_close($conn);
}

//функция вывода списка должностей
function AllTitList() {
	$conn = iconnect();
	if ($res = $conn->query('SELECT id, name from titles order by created;'))
	{
		while ($row = $res->fetch_row())
		{
			echo
			'<option value="'.$row[0].'">'.$row[1].'</option>'.PHP_EOL;
		}
	}
	mysqli_close($conn);
}

//функция вывода таблицы с подразделениями
function view_all_dep() {
	$conn = iconnect();
	$k = 1;
	if ($res = $conn->query('SELECT dt.DepName, (SELECT d.DepName from department d where d.id = dt.HeadDepID) HeadDepName
  		from department dt order by dt.id;
		'))	{
		echo '
	<div class="table-responsive">
  		<table class="table table-bordered">
    		<thead>
		      <tr class="success">
				<th>#</th>
		        <th>Название подразделения</th>
				<th>Верхнее подразделение </th>
		      </tr>
		    </thead>
		    <tbody>';
		while ($row = $res->fetch_row())
			{
				if (empty($row[1])) $row[1] = 'Наивысшее';
			echo
				'
				<tr>
					<td>'.$k++.'</td>
					<td>'.$row[0].'</td>
					<td>'.$row[1].'</td>
				</tr>
				';
		}
		echo '
		    </tbody>
  		</table>
	</div>';
	}
	mysqli_close($conn);
}

//функция вывода таблицы должностей
function view_all_tit() {
	$conn = iconnect();
	$k = 1;
	if ($res = $conn->query('SELECT name from titles order by created;'))
	{
		echo '
		<div class="table-responsive">
  		<table class="table table-bordered">
    		<thead>
		      <tr class="success">
				<th>#</th>
		        <th>Название должности</th>
		      </tr>
		    </thead>
		    <tbody>';
		while ($row = $res->fetch_row())
		{
			echo
			'
				<tr>
					<td>'.$k++.'</td>
					<td>'.$row[0].'</td>
				</tr>
				';
		}
		echo '
		    </tbody>
  		</table>
	</div>';
	}
	mysqli_close($conn);
}

//функция добавления назначения сотрудника в определённую организацию с определённой должностью
function db_ToPosition($EmpId, $DepId, $TitleId)	{
	$conn = iconnect();
	$query = 'insert into displacements (EmpId, DepId, TitId) values ('.$EmpId.', '.$DepId.', '.$TitleId.');';
	$res = $conn->query($query);
	if(!$res)	{
		alert_danger('Ошибка записи в БД запрос: </br>'.$query);
		die();
	}
	mysqli_close($conn);
}

//функция вывода таблицы сотрудников по должностям
function view_all_emp_by_tit() {
	$conn = iconnect();
	$k = 1;
	if ($res = $conn->query('select e.id, trim(concat(e.lastname, " ", e.firstname, " ", e.middlename)) fio, t.name, d.DepName, max(ds.created) from employees e
		  join displacements ds on e.id = ds.Empid
		  join titles t on t.id = ds.TitId
		  join department d on d.id = ds.DepId
		GROUP BY fio
		order by fio;'))	{
		echo '
	<div class="table-responsive">
  		<table class="table table-bordered">
    		<thead>
		      <tr class="success">
				<th>#</th>
		        <th>Сотрудник</th>
		        <th>Должность</th>
				<th>Подразделение</th>
		      </tr>
		    </thead>
		    <tbody>';
		while ($row = $res->fetch_row())
		{
			echo
			'
				<tr>
					<td>'.$k++.'</td>
					<td>'.$row[1].'</td>
					<td>'.$row[2].'</td>
					<td>'.$row[3].'</td>
				</tr>
				';
		}
		echo '
		    </tbody>
  		</table>
	</div>';
	}
	mysqli_close($conn);
}

//функция вывода таблицы всей штатной структуры
function view_tree() {
	$conn = iconnect();
	$k = 1;
	if ($res = $conn->query('select (select dep.DepName from department dep where dep.id = d.HeadDepID) HeadDep, d.DepName, 
		  trim(concat(e.lastname, " ", e.firstname, " ", e.middlename)) fio, 
		  t.name, 
		  max(ds.created) 
		from employees e
		  join displacements ds on e.id = ds.Empid
		  join titles t on t.id = ds.TitId
		  join department d on d.id = ds.DepId
		GROUP BY fio
		order by d.id;'))	{
		echo '
	<div class="table-responsive">
  		<table class="table table-bordered">
    		<thead>
		      <tr class="success">
				<th>#</th>
				<th><small>Подразделение</small></th>
				<th><small>Верхнее подразделение</small></th>
		        <th><small>Сотрудник</small></th>
				<th><small>Должность</small></th>
				<th><small>Дата назначения</small></th>
		      </tr>
		    </thead>
		    <tbody>';
		while ($row = $res->fetch_row())
		{
			$date = new DateTime($row[4]);
			if (empty($row[0])) $row[0] = 'Наивысшее';
			echo
			'
				<tr>
					<td>'.$k++.'</td>
					<td>'.$row[1].'</td>
					<td>'.$row[0].'</td>
					<td>'.$row[2].'</td>
					<td>'.$row[3].'</td>
					<td>'.$date->format('d/m/Y').'</td>
				</tr>
				';
		}
		echo '
		    </tbody>
  		</table>
	</div>';
	}
	mysqli_close($conn);
}

function db_ChangeDep($DepId, $NewName)	{
	$conn = iconnect();
	$query = 'update department d set DepName = \''.$NewName.'\' where d.id = '.$DepId.';';
	$res = $conn->query($query);
	if(!$res)	{
		alert_danger('Ошибка записи в БД запрос: </br>'.$query);
		die();
	}
	mysqli_close($conn);
}

//функция удаления подразделения
function db_DelDep($DepId)	{
	$conn = iconnect();
	$query = '
		delete from displacements where DepId = '.$DepId.';
		delete from department where id = '.$DepId.';';
	$res = $conn->multi_query($query);
	if(!$res)	{
		alert_danger('Ошибка записи в БД запрос: </br>'.$query);
		die();
	}
	mysqli_close($conn);
}

//функция вывода списка для выбора должности
function AllTitlesList() {
	$conn = iconnect();
	if ($res = $conn->query('SELECT id, name from titles order by id;'))
	{
		while ($row = $res->fetch_row())
		{
			echo
			'<option value="'.$row[0].'">'.$row[1].'</option>'.PHP_EOL;
		}
	}
	mysqli_close($conn);
}

//функция обновления изменений в должности
function db_ChangeTit($TitleId, $NewName)	{
	$conn = iconnect();
	$query = 'update titles t set t.name = \''.$NewName.'\' where t.id = '.$TitleId.';';
	$res = $conn->query($query);
	if(!$res)	{
		alert_danger('Ошибка записи в БД запрос: </br>'.$query);
		die();
	}
	mysqli_close($conn);
}

//функция удаления должности
function db_DelTit($TitleId)	{
	$conn = iconnect();
	$query = '
		delete from displacements where TitId = '.$TitleId.';
		delete from titles where id = '.$TitleId.';';
	$res = $conn->multi_query($query);
	if(!$res)	{
		alert_danger('Ошибка записи в БД запрос: </br>'.$query);
		die();
	}
	mysqli_close($conn);
}

//функция вызврата значения определённого поля сотрудника
function GetEmpDetail($EmpId, $Field) {
	$val = null;
	$conn = iconnect();
	if ($res = $conn->query('SELECT '.$Field.' from employees where id = '.$EmpId.';'))
		while ($row = $res->fetch_row())
			$val = $row[0];
	mysqli_close($conn);
	return $val;
}

//функция выбора месяца
function SelectedBirthMonth($mon)	{
	$mon = intval($mon);
		if (1 == $mon) echo '<option value="1" selected>Январь</option>'; else echo '<option value="1" >Январь</option>';
		if (2 == $mon) echo '<option value="2" selected>Февраль</option>'; else echo '<option value="2" >Февраль</option>';
		if (3 == $mon) echo '<option value="3" selected>Март</option>'; else echo '<option value="3" >Март</option>';
		if (4 == $mon) echo '<option value="4" selected>Апрель</option>'; else echo '<option value="4" >Апрель</option>';
		if (5 == $mon) echo '<option value="5" selected>Май</option>'; else echo '<option value="5" >Май</option>';
		if (6 == $mon) echo '<option value="6" selected>Июнь</option>'; else echo '<option value="6" >Июнь</option>';
		if (7 == $mon) echo '<option value="7" selected>Июль</option>'; else echo '<option value="7" >Июль</option>';
		if (8 == $mon) echo '<option value="8" selected>Август</option>'; else echo '<option value="8" >Август</option>';
		if (9 == $mon) echo '<option value="9" selected>Сентябрь</option>'; else echo '<option value="9" >Сентябрь</option>';
		if (10 == $mon) echo '<option value="10" selected>Октябрь</option>'; else echo '<option value="10" >Октябрь</option>';
		if (11 == $mon) echo '<option value="11" selected>Ноябрь</option>'; else echo '<option value="11" >Ноябрь</option>';
		if (12 == $mon) echo '<option value="12" selected>Декабрь</option>'; else echo '<option value="12" >Декабрь</option>';
}

//функция вывода списка годов
function Years()	{
	for ($i = 1955; $i <= 1995; $i ++)
	{
	echo '<option>'.$i.'</option>';
	}
}

//функция вывода списка годов для выбора
function YearsSelected($year)	{
	for ($i = 1955; $i <= 1995; $i ++)
	{
		if ($year == $i)
			echo '<option selected>'.$i.'</option>';
		else echo '<option>'.$i.'</option>';
	}
}

//функция вывода списка дней
function Days()	{
	for ($i = 1; $i <= 31; $i ++)
	{
		echo '<option>'.$i.'</option>';
	}
}

//функция обновления данных о сотруднике
function db_UpdateEmp($id,
		$LastName,
		$FirstName,
		$MiddleName,
		$Sex,
		$Day,
		$Month,
		$Year,
		$IIN,
		$address,
		$Phone,
		$Education
		)	{
			$conn = iconnect();
			$date = $Day.'.'.$Month.'.'.$Year;
			$query = 'update employees set 
		LastName = \''.$LastName.'\',
		FirstName = \''.$FirstName.'\',
		MiddleName = \''.$MiddleName.'\',
		Sex = \''.$Sex.'\',
		birthdate = \''.$date.'\',
		IIN = \''.$IIN.'\',
		address = \''.$address.'\',
		Phone = \''.$Phone.'\',
		Education = \''.$Education.'\' where id = '.$id.';';
		$res = $conn->query($query);
			if(!$res)	{
				alert_danger('Ошибка записи в БД запрос: </br>'.$query);
				die();
			}
			mysqli_close($conn);
}

//функция удаления сотрудника
function db_DelEmp($EmpId)	{
	$conn = iconnect();
	$query = '
		delete from displacements where EmpId = '.$EmpId.';
		delete from employees where id = '.$EmpId.';';
	$res = $conn->multi_query($query);
	if(!$res)	{
		alert_danger('Ошибка записи в БД запрос: </br>'.$query);
		die();
	}
	mysqli_close($conn);
}

//функция поиска по подразделениям и вывода результата в таблице 
function search_dep($param) {
	$conn = iconnect();
	$k = 1;
	if ($res = $conn->query('SELECT dt.DepName, (SELECT d.DepName from department d where d.id = dt.HeadDepID) HeadDepName
  		from department dt where dt.DepName like \'%'.$param.'%\' order by dt.id;
		'))	{
		echo '
	<div class="table-responsive">
  		<table class="table table-bordered">
    		<thead>
		      <tr class="success">
				<th>#</th>
		        <th>Название подразделения</th>
				<th>Верхнее подразделение </th>
		      </tr>
		    </thead>
		    <tbody>';
		while ($row = $res->fetch_row())
		{
			if (empty($row[1])) $row[1] = 'Наивысшее';
			echo
			'
				<tr>
					<td>'.$k++.'</td>
					<td>'.$row[0].'</td>
					<td>'.$row[1].'</td>
				</tr>
				';
		}
		echo '
		    </tbody>
  		</table>
	</div>';
	}
	mysqli_close($conn);
}

//функция поиска по должностям и вывода результата в таблице
function search_tit($param) {
	$conn = iconnect();
	$k = 1;
	if ($res = $conn->query('SELECT name from titles where name like \'%'.$param.'%\' order by created;'))
	{
		echo '
		<div class="table-responsive">
  		<table class="table table-bordered">
    		<thead>
		      <tr class="success">
				<th>#</th>
		        <th>Название должности</th>
		      </tr>
		    </thead>
		    <tbody>';
		while ($row = $res->fetch_row())
		{
			echo
			'
				<tr>
					<td>'.$k++.'</td>
					<td>'.$row[0].'</td>
				</tr>
				';
		}
		echo '
		    </tbody>
  		</table>
	</div>';
	}
	mysqli_close($conn);
}

//функция поиска по сотрудникам и вывода результата в таблице
function search_emp($param) {
	$conn = iconnect();
	$k = 1;
	if ($res = $conn->query('SELECT * from employees e
		where 
		  e.lastname like \'%'.$param.'%\' 
		  or e.firstname like \'%'.$param.'%\' 
		  or e.middlename like \'%'.$param.'%\' 
		  or e.iin like \'%'.$param.'%\' 
		  or e.sex like \'%'.$param.'%\' 
		  or e.address like \'%'.$param.'%\' 
		  or e.phone like \'%'.$param.'%\' 
		  or e.education like \'%'.$param.'%\' 
		order by created;'))	{
		echo '
				
	<div class="table-responsive">
  		<table class="table table-bordered">
    		<thead>
		      <tr class="success">
				<th>#</th>
		        <th>Фамилия</th>
		        <th>Имя</th>
				<th>Отчество</th>
				<th>Пол</th>
				<th>Дата рождения</th>
				<th>ИИН</th>
				<th>Адрес проживания</th>
				<th>Телефон</th>
				<th>Образование</th>
				<th>Дата учёта</th>
		      </tr>
		    </thead>
		    <tbody>';
		while ($row = $res->fetch_row())
		{
			$date = new DateTime($row[10]);
			$date1 = new DateTime($row[9]);
			echo
			'
				<tr>
					<td>'.$k++.'</td>
					<td>'.$row[1].'</td>
					<td>'.$row[2].'</td>
					<td>'.$row[3].'</td>
					<td>'.$row[5].'</td>
					<td>'.$date1->format('d/m/Y').'</td>
					<td>'.$row[4].'</td>
					<td>'.$row[6].'</td>
					<td>'.$row[7].'</td>
					<td>'.$row[8].'</td>
					<td>'.$date->format('d/m/Y').'</td>
				</tr>
				';
		}
		echo '
		    </tbody>
  		</table>
	</div>';
	}
	mysqli_close($conn);
}

//функция проверки логина и пароля при входе
function check_login($login, $pwd)	{
	$b = false;
	$conn = iconnect();
	if ($res = $conn->query('SELECT count(*) from users where login = \''.$login.'\' and pwd = \''.$pwd.'\';'))
		while ($row = $res->fetch_row())
			if ($row[0] > 0) $b = true;
			mysqli_free_result($res);
			mysqli_close($conn);
			return $b;
}

?>