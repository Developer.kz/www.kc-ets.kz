<?php
if (isset($_SESSION['login']))
	{
	$login = $_SESSION['login'];
	if ($_SESSION['logged'] == 'yes')
		if ($login == 'ADMIN' || $login == 'DEV')
			{
			$query = "SELECT * from msg where year(modified) = year(now()) order by modified desc;";
			}
		else
			{
			$query = "SELECT * from msg where login = '$login' and year(modified) = year(now()) 
			order by modified;";
			}
	}
$conn = iconnect();
$k = 1;
if ($res = $conn->query($query))
	{
	echo '
	<h4>Журнал отправленных документов:</h4>
	<div class="table-responsive">
  		<table class="table table-bordered">
    		<thead>
		      <tr class="success">
				<th>#</th>
				<th>Печать</th>
				<th>ID</th>
		        <th>Логин</th>
				<th>Дата и время отправки</th>
		        <th>Тело сообщения</th>
				<th>Статус проверки ЭЦП</th>
				<th>Информация о сертификате</th>
		      </tr>
		    </thead>
		    <tbody>';
		while ($row = $res->fetch_row())
			{
			$status = null;
			if ($row[7] == '1')
				$status = "Успешно";
			else 
				$status = "Ошибка";
			echo
			'
				<tr>
					<td>'.$k++.'</td>
					<td><a href="frm_msg_print.php?p=cab_print&id='.$row[0].'"><span class="glyphicon glyphicon-print"></span></a></td>
					<td>'.$row[0].'</td>
					<td class="col-md-4">'.$row[1].'</td>
					<td class="col-md-4">'.$row[6].'</td>
					<td class="col-md-4">'.$row[2].'</td>
					<td class="col-md-4">'.$status .'</td>
					<td class="col-md-4">'.$row[4].'</td>
				</tr>
				';
		}
		echo '
		    </tbody>
  		</table>
	</div>';
	}
else
	{
		alert_danger("Ошибка чтения данных. Обратитесь в техподдержку.</h4");
	}
mysqli_close($conn);
?>