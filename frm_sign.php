<div class="row">
 <p class="text-right">
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Подписать</button>
</p>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="exampleModalLabel">Электронная цифровая подпись</h4>
	      </div>
	      <div class="modal-body">
	        <!--  <form>  -->
	          <div class="form-group">
	            <label for="recipient-name" class="control-label">Тип хранилища ключа:</label>
	            <select onchange="chooseStoragePath();" id="storageAlias" size="1" style="width:100%;">
                    <option value="NONE">-- Выберите тип --</option>
                    <option value="PKCS12">Ваш Компьютер</option>
                    <option value="AKKaztokenStore">Казтокен</option>
                    <option value="AKKZIDCardStore">Личное Удостоверение</option>
                    <option value="AKEToken72KStore">EToken Java 72k</option>
                    <option value="AKJaCartaStore">AK JaCarta</option>
                </select>
	          </div>
	          <div class="form-group">
	            <label for="message-text" class="control-label">Путь хранилища ключа</label>
                <input class="form-control" id="storagePath" type="text" placeholder="" disabled>
	          </div>
	          <div class="form-group">
	            <label for="message-text" class="control-label">Пароль хранилища</label>
			    <input type="password" class="form-control" id="password" placeholder="">
	          </div>
	          <div class="form-group">
	          	<label for="message-text" class="control-label">Список ключей</label>
	            <input type="hidden" id="keyAlias" value=""/>
                <select  onchange="keysOptionChanged();" id="keys"></select>
                <input value="Обновить список ключей" onclick="fillKeys();" type="button"/>
	          </div>
	        <!-- </form> -->
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
	        <button type="button" class="btn btn-primary" onclick="SignAndVerify('AU02');">Подписать</button>
	      </div>
	    </div>
	  </div>
	</div>
</div>